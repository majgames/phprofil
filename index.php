<?php

    if (isset($_GET["username"]))
    {
        $username = $_GET["username"];
        require($username.".data.php");
        if ($promoimg == null || !isset($promoimg)) {
            if (isset($email) && $email != null) {
                $promoimghash = md5(strtolower(trim($email)));
            }
        }
        if (isset($email) && $email != null) {
            require_once("recaptchalib.php");
            $mailhide_pubkey = '01d7JyreXNF8RNG5BnbjBAiw==';
            $mailhide_privkey = '20b9016988efcc9d50e911b99e13c306';
            $emailhash = recaptcha_mailhide_url ($mailhide_pubkey, $mailhide_privkey, $email);
        }
?>
<!doctype html>
<html>
    <head>
        <title>PHProfil - <?php echo $username; ?></title>
        <meta charset="UTF-8" />
        <link rel="openid.server" href="http://id.ijestfajnie.pl/auth/" />
        <link rel="openid2.provider" href="http://id.ijestfajnie.pl/auth/" />
    </head>
    <body>
        <header>
            <h1>Profil użytkownika <?php echo $username; ?></h1>
            <h4>Automatycznie zbudowany przez PHProfil</h4>
        </header>
        <section id="avatar">
            <?php if (isset($promoimg) && $promoimg != null) { ?>
            <img src="<?php echo $promoimg; ?>" />
            <?php } elseif (isset($promoimghash) && $promoimghash != null) { ?>
            <img src="http://gravatar.com/avatar/<?php echo $promoimghash; ?>?s=200&d=retro%r=pg" />
            <?php } ?>
        </section>
        <section id="stamps">
            <table>
                <tr>
                <?php if (isset($blogger) && $blogger != null) { ?>
                    <td><a href="http://<?php echo $blogger; ?>.blogspot.com"><img src="http://www.stampaxx.com/assets/images/stamps/blogger.png" /></a></td>
                <?php } ?>
                <?php if (isset($facebook) && $facebook != null) { ?>
                    <td><a href="http://facebook.com/<?php echo $facebook; ?>"><img src="http://www.stampaxx.com/assets/images/stamps/facebook.png" /></a></td>
                <?php } ?>
                <?php if (isset($email) && $email != null) { ?>
                    <td><a href="<?php echo $emailhash; ?>" target="_blank"><img src="http://www.stampaxx.com/assets/images/stamps/mail.png" /></a></td>
                <?php } ?>
                <?php /* if (isset($picasa) && $email != null) { ?>
                    <td><a href="<?php echo $picasa; ?>"><img src="http://www.stampaxx.com/assets/images/stamps/picasa.png" /></a></td>
                <?php } */ ?>
                <?php if (isset($twitter) && $twitter != null) { ?>
                    <td><a href="http://twitter.com/<?php echo $twitter; ?>"><img src="http://www.stampaxx.com/assets/images/stamps/twitter.png" /></a></td>
                <?php } ?>
                <?php if (isset($youtube) && $youtube != null) { ?>
                    <td><a href="http://youtube.com/<?php echo $youtube; ?>"><img src="http://www.stampaxx.com/assets/images/stamps/youtube.png" /></a></td>
                <?php } ?>
                </tr>
            </table>
        </section>
        <footer>
            Copyright by &copy; 2012 MAJ Game Studios.
            <br />
            <img src="http://ijestfajnie.pl/AllRightsReshaved.png" alt="Wszystkie prawa ostrzyżone." width="15" height="15" />Wszystkie prawa ostrzyżone.
            <br />
            <br />
            Wyrażenie <strong>Wszystkie prawa ostrzyżone.</strong> wraz z symbolem <img src="http://ijestfajnie.pl/AllRightsReshaved.png" alt="Wszystkie prawa ostrzyżone." width="15" height="15" /> jest znakiem towarowym MAJ Game Studios i nie może być użyte bez wyraźnego, pisemnego pozwolenia Rady Nadzorczej MAJ Game Studios.
        </footer>
    </body>
</html>
<?php } else {
    if (!headers_sent()) {
        header("Location: http://ijestfajnie.pl");
    } else {
        die("
            <!doctype html>
            <html>
                <head>
                    <title>301 Przekierowanie</title>
                    <meta charset=\"UTF-8\" />
                </head>
                <body>
                    Buuu, nie zostałeś przekierowany? :(
                    Strona nie istnieje, idź <a href=\"http://ijestfajnie.pl\">na stronę główną</a>
                </body>
            </html>
        ");
    }
}?>